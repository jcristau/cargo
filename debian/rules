#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk
include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/buildflags.mk
include /usr/share/rustc/architecture.mk

# For cross-compiling; perhaps we should add this to dh-cargo or debcargo
export PKG_CONFIG = $(DEB_HOST_GNU_TYPE)-pkg-config
export PKG_CONFIG_ALLOW_CROSS = 1
# Needed because of https://github.com/rust-lang/cargo/issues/4133
RUSTFLAGS += -C linker=$(DEB_HOST_GNU_TYPE)-gcc
# TODO: we cannot enable this until dh_shlibdeps works correctly; atm we get:
# dpkg-shlibdeps: warning: can't extract name and version from library name 'libstd-XXXXXXXX.so'
# and the resulting cargo.deb does not depend on the correct version of libstd-rust-1.XX
# We probably need to add override_dh_makeshlibs to d/rules of rustc
#RUSTFLAGS += -C prefer-dynamic

# Pass on dpkg-buildflags stuff
RUSTFLAGS += $(foreach flag,$(LDFLAGS),-C link-arg=$(flag))
export CFLAGS CXXFLAGS CPPFLAGS LDFLAGS RUSTFLAGS

CARGO = RUST_BACKTRACE=1 cargo
CARGOFLAGS = --release --target=$(DEB_HOST_RUST_TYPE) --verbose
# Cargo looks for config in and writes cache to $CARGO_HOME/
export CARGO_HOME = $(CURDIR)/debian/cargohome

# Work around #865549, needed when using older cargo that is affected by it.
# TODO: remove after cargo/unstable is a version that was compiled with rustc
# >= 1.18 where this bug was fixed in Debian (i.e. probably after 0.20).
ifeq (0,$(shell test $$(uname -s) = "Linux" -a $$(getconf PAGESIZE) -gt 4096; echo $$?))
  SYSTEM_WORKAROUNDS += ulimit -s $$(expr $$(getconf PAGESIZE) / 1024 '*' 256 + 8192);
endif

# Disable tests on powerpc and powerpcspe for now.
# cbmuser requested this and will fix the errors later
# TODO: once powerpc powerpcspe test failures are fixed drop this
ifeq ($(DEB_HOST_ARCH), powerpc)
	DEB_BUILD_PROFILES = nocheck
endif

ifeq ($(DEB_HOST_ARCH), powerpcspe)
	DEB_BUILD_PROFILES = nocheck
endif

%:
	$(SYSTEM_WORKAROUNDS) dh $@ --with bash-completion

override_dh_auto_configure:
	# cp -a $(CURDIR)/Cargo.lock $(CURDIR)/.Cargo.lock.orig
ifneq ($(filter pkg.cargo.mkstage0,$(DEB_BUILD_PROFILES)),)
	# NOTE: this very likely doesn't work any more, see bootstrap.py for details
	# Instead, you can try to bootstrap by setting PATH to a binary cargo
	# downloaded from upstream; or by cross-compiling, see "build-cross" below
	# Preserved in case someone wants to resurrect it later:
	# Bootstrap cargo stage0
	./debian/bootstrap.py \
		--no-clean \
		--no-clone \
		--no-git \
		--no-download \
		--crate-index $(CURDIR)/vendor/index / \
		--cargo-root $(CURDIR)/ \
		--target-dir $(CURDIR)/deps \
		--host=$(DEB_HOST_RUST_TYPE) \
		--target=$(DEB_TARGET_RUST_TYPE)
	# Workaround for https://github.com/rust-lang/cargo/issues/1423
	ln -s `find $(CURDIR)/deps -name 'cargo-*' -type f -executable` $(CURDIR)/cargo-stage0
else
	ln -sf `which cargo` $(CURDIR)/cargo-stage0
endif
	debian/scripts/prune-checksums vendor/backtrace-sys-*/
	debian/scripts/prune-checksums vendor/libgit2-sys-*/

override_dh_auto_build-arch:
	$(CARGO) build $(CARGOFLAGS)

override_dh_auto_build-indep:
ifeq (,$(findstring nodoc,$(DEB_BUILD_PROFILES)))
	$(CARGO) doc $(CARGOFLAGS)
	# Extra instructions from README.md, unfortunately not done by "cargo doc"
	# sh src/ci/dox.sh
	# Post-processing for Debian
	cd target/doc/ && rm -f jquery.js && ln -s /usr/share/javascript/jquery/jquery.js
endif

override_dh_auto_test:
ifeq (,$(findstring nocheck,$(DEB_BUILD_PROFILES)))
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	CFG_DISABLE_CROSS_TESTS=1 $(CARGO) test $(CARGOFLAGS)
endif
endif

override_dh_auto_install:
	# We pick stuff directly from target/

override_dh_auto_clean:
	-mv $(CURDIR)/.Cargo.lock.orig $(CURDIR)/Cargo.lock
	$(CARGO) clean $(CARGOFLAGS)
	-$(RM) -r $(CURDIR)/target/ \
			$(CURDIR)/.cargo \
			$(CURDIR)/config.mk \
			$(CURDIR)/config.stamp \
			$(CURDIR)/Makefile \
			$(CURDIR)/Cargo.lock \
			$(CURDIR)/cargo-stage0

override_dh_clean:
	# Upstream contains a lot of these
	dh_clean -XCargo.toml.orig

CROSS_SBUILD = sbuild --profiles=nocheck \
	  --build-failed-commands '%SBUILD_SHELL' \
	  --host=$(DEB_HOST_ARCH) \
	  --no-arch-all $(EXTRA_SBUILD_FLAGS)

SBUILD_REPO_EXPERIMENTAL = --extra-repository="deb http://httpredir.debian.org/debian experimental main"

# Sometimes this is necessary, if the mirrors have different versions for different architectures
ifeq (1,$(SBUILD_USE_INCOMING))
CROSS_SBUILD += --extra-repository="deb http://incoming.debian.org/debian-buildd buildd-unstable main"
SBUILD_REPO_EXPERIMENTAL += --extra-repository="deb http://incoming.debian.org/debian-buildd buildd-experimental main"
endif

crossbuild:
	$(CROSS_SBUILD) .

crossbuild-experimental:
	$(CROSS_SBUILD) $(SBUILD_REPO_EXPERIMENTAL) --build-dep-resolver=aspcud .
