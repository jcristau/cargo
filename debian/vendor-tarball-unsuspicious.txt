# This is a list of files and dirs that are omitted from our custom
# "suspicious files" scanner

# test data
flate2-*/tests/
tar-*/tests/archives/
term-*/tests/data/
toml-*/tests/
num-*/ci/
openssl-*/test/

# misc support data
hamcrest-*/LICENSE-*
*/.travis.yml
# "build status" link-images etc take up a lot of line-length
*/README.md

# individual files, manually audited:
idna-*/tests/IdnaTest.txt
idna-*/src/uts46_mapping_table.rs
regex-*/src/testdata/basic.dat
regex-*/tests/fowler.rs
libgit2-sys-*/libgit2/src/openssl_stream.c
term-*/scripts/id_rsa.enc
url-*/github.png
num-*/doc/favicon.ico
num-*/doc/rust-logo-128x128-blk-v2.png
num-*/.travis/deploy.enc
miniz-sys-*/miniz.c
docopt-*/src/test/testcases.rs
winapi-*/src/winnt.rs
winapi-*/README.md
miniz-sys-*/miniz.c
itoa-*/performance.png
dtoa-*/performance.png
backtrace-sys-*/src/libbacktrace/configure
unicode-normalization-*/src/tables.rs
conv-*/src/errors.rs
conv-*/src/macros.rs
conv-*/src/impls.rs
conv-*/src/lib.rs
termion-*/logo.svg
tar-*/Cargo.toml
schannel-*/LICENSE.md
lazy_static-*/src/lib.rs
clap-*/.github/CONTRIBUTING.md
clap-*/CONTRIBUTORS.md
clap-*/CHANGELOG.md
vec_map-*/Cargo.toml

# test data for schanel
schannel-*/test/*

#libgit2 test files
libgit2-sys-*/libgit2/tests/filter/crlf.h
libgit2-sys-*/libgit2/tests/path/win32.c
libgit2-sys-*/libgit2/tests/status/status_data.h
libgit2-sys-*/libgit2/src/openssl-stream.c
libgit2-sys-*/libgit2/src/hash/sha1dc/ubc_check.c

# code of conduct files
failure-*/CODE_OF_CONDUCT.md
failure_derive-*/CODE_OF_CONDUCT.md
